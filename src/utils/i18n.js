import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import En from './languages/en.json'
import Ru from './languages/ru.json';

const localStorageIndex = 'language-last-index';

// //import {request} from "./actions/main";
//
// const setInLocalStorageLanguages = () => {
//
//     request('/languages', 'GET').then(response =>  response.json().then(data => {
//         let languages = [];
//         for (const key in data[0]) {
//             if ((key !== 'keyword') && (key !== 'id')) {
//                 languages.push(key);
//             }
//         }
//         const options = {};
//         languages.forEach(language => {
//             options[language] = {
//                 translation: {}
//             };
//             const translation = options[language].translation;
//             data.forEach(item => {
//                 translation[item.keyword] = item[language]
//             })
//         });
//
//         localStorage.setItem('languages', JSON.stringify(options));
//         window.location.reload();
//     }));
// }

// const getLastIndex = () => {
//
//     return;
//
//     request('languages/lastIndex').then(response => response.json().then(data => {
//         if (localStorage.getItem(localStorageIndex) != data.id) {
//             localStorage.setItem(localStorageIndex, data.id);
//             setInLocalStorageLanguages();
//         }
//     }))
// }



let languages = JSON.parse(localStorage.getItem('languages'));

if (!languages) {
   languages = {
       en: {
           translation: En
       },
       ru: {
           translation: Ru
       }
   }
}

//getLastIndex();
//
// let data = [];
// Object.keys(languages.en.translation).forEach(key => {
//     data[key] = {
//         'en': languages.en.translation[key],
//         ru: languages.ru.translation[key]
//     };
// })
//
// console.log(data);
//
// let csv = 'id;en;ru\n';
// Object.keys(data).forEach(key => csv += `${key};${data[key].en};${data[key].ru}\n`);
// console.log(csv);

i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
        resources: languages,
        lng: 'en',
        fallbackLng: "en",

        interpolation: {
            escapeValue: false
        }
    });

window.languages = languages;
window.currentLanguage = 'en';
