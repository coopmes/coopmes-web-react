import React, {useContext, useState} from "react";
import { useTheme } from "@material-ui/styles";
import { useMediaQuery } from "@material-ui/core";

const MainContext = React.createContext();

export const useMainContext = () => {
    return useContext(MainContext);
}

export function MainProvider({ children }) {
    const theme = useTheme();
    const isDesktop = useMediaQuery(theme.breakpoints.up('lg'), {
        defaultMatches: true
    });

    const [openChat, setOpenChat] = useState(isDesktop);
    const [openFriends, setOpenFriends] = useState(false);
    const [openSettings, setOpenSettings] = useState(false);

    return (
        <MainContext.Provider value={{
            openChat,
            setOpenChat,
            openFriends,
            setOpenFriends,
            openSettings,
            setOpenSettings
        }}>
            { children }
        </MainContext.Provider>
    )
}