import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';
import { setContext } from "@apollo/client/link/context";
import { WebSocketLink } from "@apollo/client/link/ws";
import { split } from '@apollo/client';
import { getMainDefinition } from "@apollo/client/utilities";

const httpLink = createHttpLink({
    uri: 'http://localhost:3000/graphql',
});

const wsLink = new WebSocketLink({
    uri: 'ws://localhost:3000/graphql',
    options: {
        reconnect: true
    }
});



const authLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists
    const token = localStorage.getItem('token');
    // return the headers to the context so httpLink can read them

    return {
        headers: {
            ...headers,
            Token: token,
        }
    }
});

const link = split(
    ({ query }) => {
        const { kind, operation } = getMainDefinition(query);
        return (
            kind === 'OperationDefinition' &&
            operation === 'subscription'
        );
    },
    wsLink,
    authLink.concat(httpLink)
);

export const client = new ApolloClient({
    cache: new InMemoryCache(),
    link
});