import React, { useState } from "react";
import { makeStyles, useTheme } from "@material-ui/styles";
import { SwipeableDrawer, useMediaQuery } from "@material-ui/core";
import BottomAppBar from "./components/BottomAppBar";
import RoomsList from "./components/RoomsList";
import TopAppBar from "./components/TopAppBar";
import { RoomProvider } from "./context/Room.context";

const useStyles = makeStyles(theme => ({
    drawer: {
        width: 360,
        [theme.breakpoints.down('md')]: {
            width: '100%'
        },
        '&.MuiDrawer-paperAnchorDockedLeft': {
            border: 'none'
        }
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%'
    },
    list: {
        flex: 1
    }
}));


export default function Rooms() {
    const classes = useStyles();

    const theme = useTheme();
    const isDesktop = useMediaQuery(theme.breakpoints.up('lg'), {
        defaultMatches: true
    });

    const [open, setOpen] = useState(true);

    return (
        <RoomProvider>
            <SwipeableDrawer
                anchor="left"
                onClose={() => setOpen(false)}
                onOpen={() => setOpen(true)}
                variant={'permanent'}
                classes={{paper: classes.drawer}}
                open={open} >

                <div className={classes.root}>
                    <TopAppBar />

                    <RoomsList className={classes.list} />

                    <BottomAppBar/>
                </div>

            </SwipeableDrawer>
        </RoomProvider>
    )
}