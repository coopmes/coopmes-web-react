import React from "react";
import { makeStyles } from "@material-ui/styles";
import { SpringOpacity, SpringSlideUp } from "../../../../components";

const useStyles = makeStyles(theme => ({
    root: {

    }
}))

export default function Message({ text, userId, createAt, self }) {
    const classes = useStyles();

    return (
        <SpringOpacity>
            <div>
                {text}
            </div>
        </SpringOpacity>
    )
}