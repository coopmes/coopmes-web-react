import React from "react";
import { Toolbar } from '@material-ui/core';
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(theme => ({
    toolbar: {
        background: theme.palette.primary.main,
        color: theme.palette.white,
        minHeight: 48,
        padding: 0
    }
}))

export default function Panel({ children, toolbarChildren }) {
    const classes = useStyles();

    return (
        <div>
            <Toolbar className={classes.toolbar}>
                {toolbarChildren}
            </Toolbar>
            <div>
                {children}
            </div>
        </div>
    )
}