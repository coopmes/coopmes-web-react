import React from "react";
import { makeStyles } from "@material-ui/styles";
import { SwipeableDrawer } from "@material-ui/core";
import { useMainContext } from "../../layouts/Main/context/Main.context";
import { FriendsProvider } from "./context/Friends.context";
import TopBar from "./components/TopBar";
import BottomBar from "./components/BottomBar";
import FriendsList from "./components/FriendsList/FriendsList";

const useStyles = makeStyles(theme => ({
    drawer: {
        width: 360,
        height: '100vh',
        [theme.breakpoints.down('md')]: {
            width: '100%'
        },
        '&.MuiDrawer-paperAnchorDockedLeft': {
            border: 'none'
        }
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%'
    },
    list: {
        flex: 1,
        overflow: "auto"
    }
}));

export default function Friends() {
    const {openFriends, setOpenFriends} = useMainContext();
    const classes = useStyles();

    return (
        <FriendsProvider>
            <SwipeableDrawer
                anchor="bottom"
                onClose={() => setOpenFriends(false)}
                onOpen={() => {}}
                variant={'temporary'}
                classes={{paper: classes.drawer}}
                open={openFriends} >

                <div className={classes.root}>
                    <TopBar />

                    <FriendsList className={classes.list} />

                    <BottomBar />
                </div>

            </SwipeableDrawer>
        </FriendsProvider>
    )
}