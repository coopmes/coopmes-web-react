import {indigo, blue, green, orange, red, blueGrey, grey, deepOrange, deepPurple} from '@material-ui/core/colors';

const white = '#FFFFFF';
const black = '#000000';
const materialShadow = '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)';

export default {
  black,
  white,
  materialShadow,
  primary: {
    contrastText: white,
    dark: indigo[900],
    main: indigo[700],
    light: indigo[500],
    menu: indigo[500]
  },
  secondary: {
    contrastText: white,
    dark: green[900],
    main: green[400],
    light: green[200]
  },
  success: {
    contrastText: white,
    dark: green[900],
    main: green[600],
    light: green[400]
  },
  info: {
    contrastText: white,
    dark: blue[900],
    main: blue[600],
    light: blue[400]
  },
  warning: {
    contrastText: white,
    dark: orange[900],
    main: orange[600],
    light: orange[400]
  },
  error: {
    contrastText: white,
    dark: red[900],
    main: red[600],
    light: red[400]
  },
  delete: {
    contrastText: white,
    dark: red[900],
    main: red[600],
    light: red[400]
  },
  text: {
    primary: blueGrey[900],
    secondary: blueGrey[600],
    link: blue[600],
    disable: grey[200]
  },
  background: {
    default: '#f9f9f9',
    paper: white
  },
  icon: blueGrey[600],
  divider: grey[200],
  disabled: grey[200]
};
