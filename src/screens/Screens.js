import React, {useEffect, useState, lazy, Suspense} from "react";
import { GET_PROFILE } from "../apollo/requests";
import { getApolloContext, useApolloClient, useLazyQuery, useQuery, useSubscription } from "@apollo/client";
import { get } from "react-hook-form";
import Main from "../layouts/Main";
import LoadingScreen from "./LoadingScreen";
import { Alert } from "@material-ui/lab";
import { useTranslation } from "react-i18next";
import { Snackbar } from "@material-ui/core";
const Login = lazy(() => import("../layouts/Login"));

export default function Screens() {
    const {t} = useTranslation();
    const [getProfile, {data, error}] = useLazyQuery(GET_PROFILE);
    const client = useApolloClient();

    // useEffect(() => {
    //     if (error) {
    //         const timeId = setTimeout(() => {
    //             getProfile()
    //         }, 1000);
    //
    //         return () => {
    //             clearTimeout(timeId);
    //         }
    //     }
    // }, [error]);

    if (error) {
        console.log(error.message)
    }


    useEffect(() => {
        getProfile();
        client.onClearStore(() => {
            getProfile();
        });
    }, []);

    if (data) {
        return (
            <Suspense fallback={<LoadingScreen />}>
                <Main />
            </Suspense>
        )
    }

    if (error) {
        return (
            <Suspense fallback={<LoadingScreen />}>
                <Login/>

                <Snackbar open={error && (error.message === 'Failed to fetch')} autoHideDuration={3000}>
                    <Alert severity="error">{t('Error connection on server')}</Alert>
                </Snackbar>
            </Suspense>
        )
    }

    return (
        <LoadingScreen />
    )

}