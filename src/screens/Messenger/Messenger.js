import React, { useEffect, useRef, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import BottomMessageSend from "./components/BottomMessageSend";
import Header from "./components/Header";
import NoHaveMessages from "./components/NoHaveMessages";
import { useLazyQuery, useMutation } from "@apollo/client";
import { GET_MESSAGES, PUSH_MESSAGE, SUBSCRIPTION_MESSAGES } from "../../apollo/requests";
import { useParams } from "react-router";
import { CircularProgress, Collapse, Fade } from "@material-ui/core";
import { MessageBox } from "react-chat-elements";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        height: '100%',
        flexDirection: 'column',
        width: '100%'
    },
    content: {
        display: 'flex',
        flexDirection: 'column',
        background: theme.palette.background.default,
        overflow: 'auto',
        flex: 1
    },
    loading: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    }
}));

const limit = 100;

export default function Messenger() {
    const classes = useStyles();
    const { id } = useParams();
    const [getMessages, { data, loading, fetchMore, subscribeToMore }] = useLazyQuery(GET_MESSAGES);
    const [offset, setOffset] = useState(0);
    const [value, setValue] = useState('');

    const bottom = useRef();

    useEffect(() => {
        setOffset(0);

        getMessages({
            variables: {
                roomId: id,
                offset: 0,
                limit
            }
        })
    }, [id]);

    useEffect(() => {
        try {
            if (subscribeToMore) {

                const unsubscribe = subscribeToMore({
                    document: SUBSCRIPTION_MESSAGES,
                    variables: {
                        roomId: id
                    },
                    updateQuery: (prev, {subscriptionData}) => {
                        if (!subscriptionData.data) {
                            return prev;
                        }

                        const {messagePushed} = subscriptionData.data;

                        return Object.assign({}, prev, {
                            getMessages: [...prev.getMessages, messagePushed]
                        });
                    }
                })

                return () => {
                    unsubscribe()
                }
            }
        } catch (e) {
            console.log(e);
        }
    }, [subscribeToMore, id, data]);


    /*
    Send message
     */
    const [pushMessage] = useMutation(PUSH_MESSAGE);
    const handleSendMessage = async () => {
        if (!value) {
            return;
        }

        await pushMessage({
            variables: {
                roomId: id,
                text: value
            }
        });

        setValue('');
    }
    /*
    ==========================
     */

    const scrollToBottomContainer = (behavior = 'smooth') => {
        bottom.current.scrollIntoView({behavior});
    }

    useEffect(() => {
        if (data && data.getMessages) {
            scrollToBottomContainer('auto');
        }
    }, [data]);

    return (
        <div className={ classes.root }>
            <Header/>

            <div className={ classes.content }>
                <Collapse in={loading}>
                    <div className={classes.loading}>
                        <CircularProgress />
                    </div>
                </Collapse>


                {
                    data && (data.getMessages.length === 0) && (
                        <NoHaveMessages />
                    )
                }

                <div>
                    {
                        data && data.getMessages.map(m => (
                            <MessageBox key={ m._id } title={ m.userId } text={ m.text }
                                        date={ new Date(m.createAt) }/>
                        )).reverse()
                    }
                    <div ref={ bottom } style={ {float: "left", clear: "both"} }></div>
                </div>

            </div>

            <BottomMessageSend value={value}
                               onSend={handleSendMessage}
                               onChange={e => setValue(e.target.value)}  />
        </div>
    )
}