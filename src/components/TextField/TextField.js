import React from "react";
import { FormControl, TextField as _TextField } from "@material-ui/core";

export default function TextField(props) {
    return (
        <FormControl fullWidth>
            <_TextField label="Standard" {...props} />
        </FormControl>
    )
}