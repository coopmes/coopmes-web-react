import React, { useEffect } from "react";
import clsx from "clsx";
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import { useLazyQuery } from "@apollo/client";
import { GET_ROOMS, GET_USERS } from "../../../../apollo/requests";
import FriendListItem from "../FriendListItem";
import { useFriendsContext } from "../../context/Friends.context";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        //maxWidth: '36ch',
        backgroundColor: theme.palette.background.paper,
    },
    list: {

    },
    inline: {
        display: 'inline',
    },
    bottom: {
        height: 200
    }
}));

export default function FriendsList({ className }) {
    const classes = useStyles();
    const [getRooms, { data, error }] = useLazyQuery(GET_USERS);
    const { search } = useFriendsContext();


    useEffect(() => {
        getRooms({
            variables: {
                query: search
            }
        });
    }, [search]);


    return (
        <div className={ clsx(className) }>
            <List className={ classes.root }>
                {
                    data && data.getUsers.map(({ _id, name, email, lastMessage }) => (
                        <FriendListItem id={_id} key={ _id } title={ email } lastMessage={ lastMessage } time={ '17:43' }/>
                    ))
                }

                <div className={classes.bottom}>

                </div>
            </List>
        </div>
    )
}