import React from "react";
import Skeleton from '@material-ui/lab/Skeleton';

const SkeletonLoading = (item, show, props) => {
    if (!item) {
        return <Skeleton variant={props.variant || 'text'} {...props}></Skeleton>
    }

    if (typeof show === 'function') {
        return show();
    } else {
        return show;
    }
}

export default SkeletonLoading;