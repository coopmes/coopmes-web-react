import React, {useContext, useState} from "react";
import { useTheme } from "@material-ui/styles";
import { useMediaQuery } from "@material-ui/core";

const FriendsContext = React.createContext();

export const useFriendsContext = () => {
    return useContext(FriendsContext);
}

export function FriendsProvider({ children }) {
    const [search, setSearch] = useState('');

    return (
        <FriendsContext.Provider value={{
            search,
            setSearch
        }}>
            { children }
        </FriendsContext.Provider>
    )
}