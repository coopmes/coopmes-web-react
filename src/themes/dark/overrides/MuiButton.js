import palette from "../palette";

export default {
  contained: {
    boxShadow: 'none',
    backgroundColor: '#FFFFFF',
  },
  containedPrimary: {
    color: palette.button.text
  },
  containedSecondary: {
    color: palette.button.text
  }
};
