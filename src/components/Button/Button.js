import React from "react";
import {Button as _Button} from "@material-ui/core";

export default function Button({children, ...other}) {
    return <_Button variant={"contained"} color={'primary'} {...other}>{children}</_Button>
}