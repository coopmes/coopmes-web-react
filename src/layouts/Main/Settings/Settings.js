import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {makeStyles, useTheme} from '@material-ui/styles';
import {useTranslation} from "react-i18next";
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Panel from "../../../components/Panel";
import { Divider, IconButton, List, ListItem, ListItemIcon, ListItemText, Typography } from "@material-ui/core";
import { useMainContext } from "../context/Main.context";
import MenuIcon from "@material-ui/icons/Menu";
import { useLazyQuery, useQuery } from "@apollo/client";
import { GET_PROFILE } from "../../../apollo/requests";
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import PeopleIcon from '@material-ui/icons/People';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

const useStyles = makeStyles(theme => ({
    drawer: {
        width: 300,
        [theme.breakpoints.up('lg')]: {
            //height: 'calc(100% - 96px)',
            borderRadius: 0,
            //border: theme.palette.borderStyle
        },
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%'
    },
    divider: {
        //margin: theme.spacing(2, 0)
    },
    nav: {
        marginBottom: theme.spacing(2)
    },
    logoutBtnIcon: {
        marginRight: theme.spacing(1)
    },
    toolbar: {
        display: 'flex',
        width: '100%',
        height: 100,
        flexDirection: 'column'
    },
    grow: {
        flexGrow: 1,
    },
    toolbarHeader: {
        display: "flex",
        flex: 1,
    },
    toolbarContent: {
        padding: 10
    }
}));

const Settings = props => {
    const {data, error} = useQuery(GET_PROFILE);

    const {openSettings, setOpenSettings, setOpenFriends} = useMainContext();

    const {variant, onClose, className, ...rest} = props;

    const classes = useStyles();

    const {t} = useTranslation();

    const clickClose = () => {
        setOpenSettings(false);
    }

    const clickOpenFriends = () => {
        setOpenFriends(true);
    }

    return (
        <SwipeableDrawer
            anchor="right"
            classes={{paper: classes.drawer}}
            onClose={() => setOpenSettings(false)}
            open={openSettings}
            variant={variant}
        >
            <div
                {...rest}
                className={clsx(classes.root, className)}
            >
                <Panel toolbarChildren={(
                    <div className={classes.toolbar}>
                        <div className={classes.toolbarHeader}>
                            <div className={classes.grow}></div>
                            <IconButton aria-label="delete" color={ 'inherit' } onClick={clickClose}>
                                <ChevronRightIcon/>
                            </IconButton>
                        </div>
                        <div className={classes.toolbarContent}>
                            <Typography color={'inherit'} variant={'h4'}>{
                                data && data.getProfile.name
                            }</Typography>
                        </div>
                    </div>
                )}>

                    <List component="nav" aria-label="main mailbox folders">
                        <ListItem button onClick={clickOpenFriends}>
                            <ListItemIcon>
                                <PeopleIcon />
                            </ListItemIcon>
                            <ListItemText secondary={t('Friends')} />
                        </ListItem>

                        <Divider></Divider>

                        <ListItem button>
                            <ListItemIcon>
                                <ExitToAppIcon />
                            </ListItemIcon>
                            <ListItemText secondary={t('Exit')} />
                        </ListItem>
                    </List>

                </Panel>
            </div>
        </SwipeableDrawer>
    );
};

Settings.propTypes = {
    className: PropTypes.string,
    onClose: PropTypes.func,
    variant: PropTypes.string.isRequired
};

export default Settings;
