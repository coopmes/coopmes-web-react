import React from "react";
import { makeStyles } from "@material-ui/styles";
import { Avatar, Divider, Typography } from "@material-ui/core";
import { useHistory } from "react-router";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        height: 62,
        padding: '7px 16px',
        '&:hover': {
            cursor: 'pointer',
            background: theme.palette.background.default
        }
    },
    title: {
        color: '#222',
        fontSize: 14
    },
    subTitle: {
        color: 'grey',
        fontSize: 13
    },
    avatarDiv: {
        flex: 1,
        marginRight: 15
    },
    content: {
        flex: 4,
        display: 'flex',
        flexDirection: 'column'
    },
    contentHeader: {
        display: 'flex',
        justifyContent: 'space-between'
    },
}))

export default function ChatField({ title, subTitle, id }) {
    const classes = useStyles();

    const history = useHistory();

    const handleGoChat = () => {
        history.push(`/rooms/${id}`)
    }

    return (
        <div className={classes.root} key={id} onClick={handleGoChat}>
            <div className={classes.avatarDiv}>
                <Avatar>H</Avatar>
            </div>
            <div className={classes.content}>
                <div className={classes.contentHeader}>
                    <Typography variant={'body1'} className={classes.title}>{title}</Typography>
                </div>
                <div>
                    <Typography variant={'body2'} className={classes.subTitle}>{subTitle}</Typography>
                </div>
            </div>
        </div>
    )
}