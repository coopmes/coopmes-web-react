import React from "react";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import ListItem from "@material-ui/core/ListItem";
import { makeStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import { useHistory } from "react-router";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',

        '&:hover': {
            cursor: 'pointer',
            background: theme.palette.listItem.hover,
            transition: '100ms'
        }
    },
}));

export default function RoomListItem({ title, lastMessage, time, id }) {
    const classes = useStyles();
    const history = useHistory();

    const onClick = () => {
        history.push(`/rooms/${id}`)
    }

    return (
        <>
            <ListItem button
                      onClick={onClick}
                      alignItems="flex-start"
                      className={ classes.root }>
                <ListItemAvatar>
                    <Avatar alt="Remy Sharp"/>
                </ListItemAvatar>
                <ListItemText
                    primary={
                        <>
                            <Typography> { title } </Typography>
                        </>
                    }
                    secondary={
                        <>
                            <Typography
                                component="span"
                                variant="body2"
                                color="textPrimary"
                            >
                                { lastMessage }
                            </Typography>
                        </>
                    }
                />

                <ListItemText primary={
                    <>
                        <Typography color="textPrimary" align={ 'right' } variant={ 'body2' }> { time } </Typography>
                    </>
                }/>
            </ListItem>
            <Divider variant="inset" component="li"/>
        </>

    )
}