import React, { useState } from "react";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import ListItem from "@material-ui/core/ListItem";
import { makeStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import { useHistory } from "react-router";
import { useLazyQuery, useMutation, useQuery } from "@apollo/client";
import { CREATE_CHAT, GET_PROFILE } from "../../../../apollo/requests";
import { useMainContext } from "../../../../layouts/Main/context/Main.context";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',

        '&:hover': {
            cursor: 'pointer',
            background: theme.palette.listItem.hover,
            transition: '100ms'
        }
    },
}));

export default function FriendListItem({ title, lastMessage, time, id }) {
    const {data: profile} = useQuery(GET_PROFILE);
    const classes = useStyles();
    const history = useHistory();
    const [createChat] = useMutation(CREATE_CHAT);
    const {setOpenFriends} = useMainContext();

    const onClick = async () => {
        const {_id: profileId} = profile.getProfile;

        const {data} = await createChat({
            variables: {
                users: [profileId, id]
            }
        });

        if (data && data.createRoom) {
            history.push(`/rooms/${data.createRoom._id}`);
            setOpenFriends(false);
        }
    }

    return (
        <>
            <ListItem button
                      onClick={onClick}
                      alignItems="flex-start"
                      className={ classes.root }>
                <ListItemAvatar>
                    <Avatar alt="Remy Sharp"/>
                </ListItemAvatar>
                <ListItemText
                    primary={
                        <>
                            <Typography> { title } </Typography>
                        </>
                    }
                    secondary={
                        <>
                            <Typography
                                component="span"
                                variant="body2"
                                color="textPrimary"
                            >
                                { lastMessage }
                            </Typography>
                        </>
                    }
                />

            </ListItem>
            <Divider variant="inset" component="li"/>
        </>

    )
}