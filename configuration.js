class Configuration {
    constructor() {
        const fs = require("fs");
        const jsonContent = JSON.parse(fs.readFileSync("config.json"));
        this.api = jsonContent.api;
    }

    getBackendUrl() {
        return this.api;
    }
}

module.exports = Configuration;