import React, { useState } from "react";
import { makeStyles } from "@material-ui/styles";
import { FormControl, Collapse, Typography, Grid, Divider } from '@material-ui/core';
import { useTranslation } from "react-i18next";
import { useForm } from "react-hook-form";
import { Button, SpringOpacity, TextField } from "../../components";
import { useHistory } from "react-router";

const useStyles = makeStyles(theme => ({
    root: {
        background: theme.palette.background.default,
        [theme.breakpoints.down('md')]: {
            background: theme.palette.background.paper
        },
        display: "flex",
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
    },
    form: {
        display: 'flex',
        background: theme.palette.background.paper,
        width: 400,
        maxWidth: 600,
        padding: 40,
        flexDirection: 'column'
    },
    grid: {
        flex: 1
    }
}))

export default function ConfirmEmail() {
    const classes = useStyles();

    const {t} = useTranslation();

    const history = useHistory();

    const handleSignIn = () => {
        history.replace('/login');
    }

    return (
        <SpringOpacity className={ classes.root }>
            <form className={ classes.form } >
                <Grid className={classes.grid} container spacing={2}>
                    <Grid item xs={12}>
                        <Typography variant={'h4'}>{t('Confirm your email')}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid item xs={12}>
                            <Button color={'primary'} onClick={handleSignIn} fullWidth>{t('Back to sign in')}</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </form>
        </SpringOpacity>
    )
}