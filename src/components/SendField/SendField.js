import React, { useRef } from "react";
import InputBase from '@material-ui/core/InputBase';
import { fade, makeStyles } from '@material-ui/core/styles';
import { useTranslation } from "react-i18next";
import { IconButton } from "@material-ui/core";
import SendIcon from '@material-ui/icons/Send';
import { useForm } from "react-hook-form";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flex: 1,
        alignItems: 'center'
    },
    inputContainer: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        flex: 1,
        color: theme.palette.input.color,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(1),
            width: 'auto',
        },
    },
    sendIcon: {},
    inputRoot: {
        color: 'inherit',
        minHeight: 36,
        width: '100%'
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 1),
        transition: theme.transitions.create('height'),
        width: '100%',
    },
}));

export default function SendField({ value, onChange, onSend }) {
    const classes = useStyles();
    const { t } = useTranslation();
    const input = useRef();
    const { handleSubmit } = useForm();

    const handleSendMessage = () => {
        if (!value) {
            return;
        }

        onSend(value);
        onChange('');

        input.current.focus();
    }

    const handleKeyDown = event => {
        if (event.keyCode === 13 && event.shiftKey) {
            return;
        }

        if (event.which === 13) {
            input.current.getElementsByTagName("textarea")[0].style.height = "auto"; //<------resize text area
            handleSendMessage();
            event.preventDefault();
        }
    };

    return (
        <form className={ classes.root } onSubmit={ handleSubmit(handleSendMessage) }>
            <div className={ classes.inputContainer }>
                <InputBase
                    placeholder={ t('Write message…') }
                    value={ value }
                    onChange={ onChange }
                    multiline
                    onKeyDown={ handleKeyDown }
                    ref={ input }
                    classes={ {
                        root: classes.inputRoot,
                        input: classes.inputInput,
                    } }
                    inputProps={ { 'aria-label': 'search' } }
                />
            </div>
            <IconButton className={ classes.sendIcon }>
                <SendIcon/>
            </IconButton>
        </form>
    )
}