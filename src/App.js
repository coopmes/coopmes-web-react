import React, {Component} from 'react'
import './App.css';
import {ThemeProvider} from '@material-ui/styles';
import light from './themes/light';
import dark from './themes/dark';
import {init} from "./utils/DatePrototype";
import { ApolloProvider } from '@apollo/client';
import 'react-chat-elements/dist/main.css';

import {client} from "./apollo/client";

import {Screens} from "./screens";
init();

class App extends Component {
    render() {
        return (
            <ThemeProvider theme={dark}>
                <ApolloProvider client={client}>
                    <Screens />
                </ApolloProvider>
            </ThemeProvider>
        );
    }

}

export default App;
