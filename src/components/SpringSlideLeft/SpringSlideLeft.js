import React, {useEffect} from "react";
import {useSpring, animated} from "react-spring";
import PropTypes from 'prop-types';

SpringSlideLeft.propTypes = {
    children: PropTypes.node,
    spring: PropTypes.number,
    show: PropTypes.bool
}

SpringSlideLeft.defaultProps = {
    spring: 200,
    show: true
}

const hiddeStyle = (spring) => ({
    transform: `translate(${spring}px, 0)`,
    height: 0,
    opacity: 0
});

const showStyle = {
    opacity: 1,
    height: 'auto',
    transform: 'translate(0, 0)'
}

export default function SpringSlideLeft({children, spring, show, ...other}) {
    const [style, set, stop] = useSpring(() => hiddeStyle(spring))

    useEffect(() => {

        set(show ? showStyle : hiddeStyle(spring));

    }, [show]);

    return (
        <animated.div style={style} {...other}>
            {
                show && children
            }
        </animated.div>
    )
}