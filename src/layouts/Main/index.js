import React from "react";
import Main from './Main';
import {MainProvider} from "./context/Main.context";

export default function Main_() {
    return (
        <MainProvider>
            <Main />
        </MainProvider>
    )
}