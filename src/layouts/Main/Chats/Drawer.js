import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {makeStyles, useTheme} from '@material-ui/styles';
import {useTranslation} from "react-i18next";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Panel from "../../../components/Panel";
import { Avatar, IconButton, Typography } from "@material-ui/core";
import { useLazyQuery } from "@apollo/client";
import { AUTH, GET_ROOMS } from "../../../apollo/requests";
import MenuIcon from '@material-ui/icons/Menu';
import { useHistory } from "react-router";
import { ChatItem } from 'react-chat-elements'
import { useMainContext } from "../context/Main.context";

const useStyles = makeStyles(theme => ({
    drawer: {
        width: 254,
        [theme.breakpoints.up('lg')]: {
            //height: 'calc(100% - 96px)',
            borderRadius: 0,
            //border: theme.palette.borderStyle
        },
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%'
    },
    divider: {
        //margin: theme.spacing(2, 0)
    },
    nav: {
        marginBottom: theme.spacing(2)
    },
    logoutBtnIcon: {
        marginRight: theme.spacing(1)
    }
}));

const Drawer = props => {
    const {openChat, setOpenChat, setOpenFriends, setOpenSettings} = useMainContext();

    const {variant, onClose, className, ...rest} = props;

    const classes = useStyles();

    const {t} = useTranslation();

    const theme = useTheme();
    const isDesktop = useMediaQuery(theme.breakpoints.up('lg'), {
        defaultMatches: true
    });

    const [getRooms, { data, error }] = useLazyQuery(GET_ROOMS);

    const history = useHistory();

    useEffect(() => {
        getRooms({
            variables: {
                offset: 0,
                limit: 50
            }
        });
    }, []);

    const handleGoChat = (room) => {
        history.push(`/rooms/${room._id}`, {
            room
        })
    }

    const handlerCloseDrawer = () => {
        if (!isDesktop) {
            onClose();
        }
    }

    const clickMenu = () => {
        setOpenSettings(true);
    }

    return (
        <SwipeableDrawer
            anchor="left"
            classes={{paper: classes.drawer}}
            onClose={() => setOpenChat(false)}
            open={openChat}
            onOpen={() => setOpenChat(true)}
            variant={variant}
        >
            <div
                {...rest}
                className={clsx(classes.root, className)}
            >
                <Panel toolbarChildren={(
                    <>
                        <IconButton aria-label="delete" color={ 'inherit' } onClick={clickMenu}>
                            <MenuIcon/>
                        </IconButton>
                       <Typography color={'inherit'} variant={'h4'}>{t('Chats')}</Typography>
                   </>
                )}>
                    {
                        data && data.getRooms.map(room => {
                            return (
                                <ChatItem key={room._id}
                                          avatar={<Avatar />}
                                          title={room.name}
                                          subtitle={room.lastMessage}
                                          onClick={e => handleGoChat(room)}
                                          date={room.lastMessageDate}></ChatItem>
                            )
                        })
                    }

                </Panel>
            </div>
        </SwipeableDrawer>
    );
};

Drawer.propTypes = {
    className: PropTypes.string,
    onClose: PropTypes.func,
    variant: PropTypes.string.isRequired
};

export default Drawer;
