import React from "react";
import {Collapse, FormControl} from "@material-ui/core";
import FormHelperText from "@material-ui/core/FormHelperText";
import PropTypes from 'prop-types';
import {makeStyles} from "@material-ui/styles";
import clsx from "clsx";

ErrorHelper.propTypes = {
    open: PropTypes.bool,
    children: PropTypes.node,
    className: PropTypes.string
}

ErrorHelper.defaultProps = {
    open: true
}

const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: theme.palette.error.light,
        padding: 15,
        borderRadius: 5,
        fontSize: 14,
        lineHeight: '135%',
        color: theme.palette.black,
    }
}))

export default function ErrorHelper({open, children, className}) {
    const classes = useStyles();

    return (
        <Collapse in={open}>
            <FormHelperText className={clsx(classes.root, className)}>
                {children}
            </FormHelperText>
        </Collapse>
    )
}