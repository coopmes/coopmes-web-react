import React, {useEffect} from "react";
import {useSpring, animated} from "react-spring";
import PropTypes from 'prop-types';

SpringOpacity.propTypes = {
    children: PropTypes.node,
    spring: PropTypes.number,
    show: PropTypes.bool
}

SpringOpacity.defaultProps = {
    spring: 200,
    show: true
}

const hiddeStyle = (spring) => ({
    opacity: 0
});

const showStyle = {
    opacity: 1
}

export default function SpringOpacity({children, spring, show, ...other}) {
    const [style, set, stop] = useSpring(() => hiddeStyle(spring))

    useEffect(() => {

        set(show ? showStyle : hiddeStyle(spring));

    }, [show]);

    return (
        <animated.div style={style} {...other}>
            {
                show && children
            }
        </animated.div>
    )
}