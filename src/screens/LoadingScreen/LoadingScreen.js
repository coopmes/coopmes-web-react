import React from "react";
import { makeStyles } from "@material-ui/styles";
import { CircularProgress } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    root: {
        background: theme.palette.background.default,
        [theme.breakpoints.down('md')]: {
            background: theme.palette.background.paper
        },
        display: "flex",
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
    },
}))

export default function LoadingScreen() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <CircularProgress color={'primary'} />
        </div>
    )
}