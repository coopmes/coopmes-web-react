import React from "react";
import { makeStyles } from "@material-ui/styles";
import MessageSent from '../../../../images/Message Sent.svg';
import { Typography } from "@material-ui/core";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column'
    },
    text: {
        color: theme.palette.text.primary
    }
}))

export default function NoHaveMessages() {
    const { t } = useTranslation();
    const classes = useStyles();

    return (
        <div className={ classes.root }>
            <img src={ MessageSent }/>

            <Typography variant={'h4'} className={classes.text}>
                {
                    t('There are no messages in this chat yet.')
                }
            </Typography>
        </div>
    )
}