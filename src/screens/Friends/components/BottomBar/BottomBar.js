import React, { useState } from "react";
import { makeStyles } from "@material-ui/styles";
import { AppBar, Fab, IconButton, Toolbar } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useMainContext } from "../../../../layouts/Main/context/Main.context";

const useStyles = makeStyles((theme) => ({
    root: {
        top: 'auto',
        bottom: 0,
        flex: 0,
        background: theme.palette.background.second,
    },
    text: {
        padding: theme.spacing(2, 2, 0),
    },
    paper: {
        paddingBottom: 50,
    },
    list: {
        marginBottom: theme.spacing(2),
    },
    subheader: {
        backgroundColor: theme.palette.background.paper,
    },
    appBar: {
        top: 'auto',
        bottom: 0,
        minHeight: 56,
    },
    grow: {
        flexGrow: 1,
    },
    fabButton: {
        position: 'absolute',
        zIndex: 1,
        //top: -30,
        left: 0,
        right: 0,
        margin: '0 auto',
        bottom: 20,
        // boxShadow: `0 0 0 8px ${theme.palette.background.paper}`,
    },
}));

export default function BottomBar() {
    const classes = useStyles();
    const {setOpenFriends} = useMainContext();

    const onClickAdd = () => {
        setOpenFriends(false);
    }

    return (
        <div className={classes.root}>
            <Fab color={'primary'}
                 aria-label="add"
                 onClick={onClickAdd}
                 className={classes.fabButton}>
                <ExpandMoreIcon color={'inherit'} />
            </Fab>
        </div>
    )
}