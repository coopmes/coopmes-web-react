import React, { useState } from "react";
import { makeStyles } from "@material-ui/styles";
import { AppBar, Fab, IconButton, Toolbar } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import AddIcon from '@material-ui/icons/Add';
import { useMainContext } from "../../../../layouts/Main/context/Main.context";

const useStyles = makeStyles((theme) => ({
    root: {
        top: 'auto',
        bottom: 0,
        flex: 0,
        background: theme.palette.background.second,
    },
    text: {
        padding: theme.spacing(2, 2, 0),
    },
    paper: {
        paddingBottom: 50,
    },
    list: {
        marginBottom: theme.spacing(2),
    },
    subheader: {
        backgroundColor: theme.palette.background.paper,
    },
    appBar: {
        top: 'auto',
        bottom: 0,
        minHeight: 56,
    },
    grow: {
        flexGrow: 1,
    },
    fabButton: {
        position: 'absolute',
        zIndex: 1,
        top: -30,
        left: 0,
        right: 0,
        margin: '0 auto',
       // boxShadow: `0 0 0 8px ${theme.palette.background.paper}`,
    },
}));

export default function BottomAppBar() {
    const classes = useStyles();
    const {setOpenFriends} = useMainContext();

    const onClickAdd = () => {
        setOpenFriends(true);
    }

    return (
        <div className={classes.root}>
            <Toolbar className={classes.appBar}>
                {/*<IconButton edge="start" color="inherit" aria-label="open drawer">*/}
                {/*    <MenuIcon />*/}
                {/*</IconButton>*/}
                <Fab color={'primary'}
                     aria-label="add"
                     onClick={onClickAdd}
                     className={classes.fabButton}>
                    <AddIcon color={'inherit'} />
                </Fab>
                <div className={classes.grow} />
                {/*<IconButton color="inherit">*/}
                {/*    <SearchIcon />*/}
                {/*</IconButton>*/}
                {/*<IconButton edge="end" color="inherit">*/}
                {/*    <MoreIcon />*/}
                {/*</IconButton>*/}
            </Toolbar>
        </div>
    )
}