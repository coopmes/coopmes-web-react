import React from "react";
import { Toolbar } from "@material-ui/core";
import { SendField } from "../../../../components";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
    root: {
        top: 0,
        bottom: 'auto',
        flex: 0,
        background: theme.palette.background.second,
    },
    appBar: {
        top: 'auto',
        bottom: 0,
        minHeight: 56,
    },
}));

export default function Header() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Toolbar className={classes.appBar}>

            </Toolbar>
        </div>
    )
}