import palette from "../palette";

export default {
    underline: {
        '&:before': {
            borderBottom: `1px solid ${palette.underline}`
        },
    }
}