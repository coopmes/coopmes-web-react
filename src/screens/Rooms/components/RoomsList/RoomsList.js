import React, { useEffect } from "react";
import clsx from "clsx";
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import RoomListItem from "../RoomListItem";
import { useLazyQuery } from "@apollo/client";
import { GET_ROOMS } from "../../../../apollo/requests";
import { useRoomContext } from "../../context/Room.context";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        //maxWidth: '36ch',
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: 'inline',
    },
}));

export default function RoomsList({ className }) {
    const classes = useStyles();
    const [getRooms, { data, error }] = useLazyQuery(GET_ROOMS);
    const { search } = useRoomContext();


    useEffect(() => {
        getRooms({
            variables: {
                offset: 0,
                limit: 50
            }
        });
    }, []);


    return (
        <div className={ clsx(className) }>
            <List className={ classes.root }>
                {
                    data && data.getRooms.map(({ _id, name, lastMessage }) => (
                        <RoomListItem id={_id} key={ _id } title={ name } lastMessage={ lastMessage } time={ '17:43' }/>
                    ))
                }
            </List>
        </div>
    )
}