import {indigo, blue, green, orange, red, blueGrey, grey, deepOrange, deepPurple, teal} from '@material-ui/core/colors';

const white = '#FFFFFF';
const black = '#000000';
const materialShadow = '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)';

export default {
  black,
  white,
  materialShadow,
  input: {
    color: white
  },
  primary: {
    contrastText: white,
    dark: teal['A700'],
    main: teal['A400'],
    light: teal['A200'],
    menu: teal[500]
  },
  secondary: {
    contrastText: white,
    dark: green[700],
    main: green[400],
    light: green[200]
  },
  success: {
    contrastText: white,
    dark: green[900],
    main: green[600],
    light: green[400]
  },
  info: {
    contrastText: white,
    dark: blue[900],
    main: blue[600],
    light: blue[400]
  },
  warning: {
    contrastText: white,
    dark: orange[900],
    main: orange[600],
    light: orange[400]
  },
  error: {
    contrastText: white,
    dark: red[900],
    main: red[700],
    light: red[400]
  },
  delete: {
    contrastText: white,
    dark: red[900],
    main: red[600],
    light: red[400]
  },
  text: {
    primary: white,
    secondary: blueGrey[600],
    link: blue[600],
    disable: grey[200]
  },
  background: {
    default: '#35363A',
    paper: '#36393F',
    second: '#2C2F33'
  },
  button: {
      text: black
  },
  underline: '#5f5f5f',
  icon: '#C4C4C4',
  divider: grey[600],
  disabled: grey[200],
  listItem: {
    hover: '#2f3a3a'
  }
};
