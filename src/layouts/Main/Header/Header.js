import React from "react";
import { Button } from "../../../components";
import { BrowserRouter } from "react-router-dom";
import { resetApolloContext, useLazyQuery, useQuery, ApolloClient, useApolloClient } from "@apollo/client";
import { AUTH, GET_PROFILE } from "../../../apollo/requests";
import { useHistory } from "react-router";

export default function Header() {
    const {data} = useQuery(GET_PROFILE);
    const [login] = useLazyQuery(AUTH);
    const history = useHistory();

    const client  = useApolloClient();

    const handleExitApp = async () => {
        localStorage.setItem('token', '');
        await client.clearStore();
        history.replace('/');
    }

    return (
       <div>
           {/*<Button onClick={handleExitApp}>Exit</Button>*/}
       </div>
    )
}