import React, { useState } from "react";
import { makeStyles } from "@material-ui/styles";
import { FormControl, Collapse, Typography, Grid, Divider } from '@material-ui/core';
import { useTranslation } from "react-i18next";
import { useForm } from "react-hook-form";
import { Button, ErrorHelper, SpringOpacity, SpringSlideUp, TextField } from "../../components";
import { useHistory } from "react-router";
import { useMutation } from "@apollo/client";
import { CREATE_USER } from "../../apollo/requests";

const useStyles = makeStyles(theme => ({
    root: {
        background: theme.palette.background.default,
        [theme.breakpoints.down('md')]: {
            background: theme.palette.background.paper
        },
        display: "flex",
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
    },
    form: {
        display: 'flex',
        background: theme.palette.background.paper,
        [theme.breakpoints.up('md')]: {
            width: 400,
        },
        maxWidth: 600,
        padding: 40,
        flexDirection: 'column'
    },
    grid: {
        flex: 1
    }
}))

export default function Login() {
    const classes = useStyles();
    const {handleSubmit, register, errors, watch} = useForm();
    const [goRegistration, {data, error}] = useMutation(CREATE_USER);

    const {t} = useTranslation();

    const history = useHistory();

    const onSubmit = async (data) => {
        try {
            await goRegistration({
                variables: data
            });
            history.push('/confirm');
        } catch (e) {
            console.log(e);
        }
    }

    const handleSignIn = () => {
        history.replace('/login');
    }

    return (
        <div className={ classes.root }>
            <SpringSlideUp>
                <form className={ classes.form } onSubmit={ handleSubmit(onSubmit) }>
                    <Grid className={classes.grid} container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                label={ t('Email') }
                                name={'email'}
                                inputRef={register({
                                    required: "Required"
                                })}/>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                label={ t('Username') }
                                name={'name'}
                                inputRef={register({
                                    required: "Required"
                                })}/>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                type={'password'}
                                name={'password'}
                                label={ t('Password') }
                                inputRef={register({
                                    required: "Required"
                                })}/>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                type={'password'}
                                name={'re-password'}
                                label={ t('Retry password') }
                                inputRef={register({
                                    required: "Required"
                                })}/>
                        </Grid>
                        <Grid item xs={12}>
                            <Button fullWidth
                                    type={'submit'}
                                    disabled={!(watch('name') && watch('email') && watch('password') && (watch('password') === watch('re-password')))}
                                    color={'secondary'}>{t('Register')}</Button>

                        </Grid>
                        <Grid item xs={12}>
                            <Divider />
                        </Grid>
                        <Grid item xs={12}>
                            <Button color={'primary'} onClick={handleSignIn} fullWidth>{t('Back to sign in')}</Button>
                        </Grid>
                        <Grid item xs={12}>
                            <Collapse in={error}>
                                <ErrorHelper>
                                    <Typography color={'inherit'}>{t('This email exist in the system')}</Typography>
                                </ErrorHelper>
                            </Collapse>
                        </Grid>
                    </Grid>
                </form>
            </SpringSlideUp>
        </div>
    )
}