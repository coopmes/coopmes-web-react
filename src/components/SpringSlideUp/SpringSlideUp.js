import React, {useEffect} from "react";
import {useSpring, animated} from "react-spring";
import PropTypes from 'prop-types';

SpringSlideUp.propTypes = {
    children: PropTypes.node,
    spring: PropTypes.number,
    show: PropTypes.bool
}

SpringSlideUp.defaultProps = {
    spring: 100,
    show: true
}

const hiddeStyle = (spring) => ({
    transform: `translate(0, ${spring}px)`,
    height: 0,
    opacity: 0
});

const showStyle = {
    opacity: 1,
    height: 'auto',
    transform: 'translate(0, 0)'
}

export default function SpringSlideUp({children, spring, show, rerenderPoint, ...other}) {
    const [style, set, stop] = useSpring(() => hiddeStyle(spring))

    useEffect(() => {

        set(show ? showStyle : hiddeStyle(spring));

    }, [show]);

    useEffect(() => {
        set(hiddeStyle(spring));

        setTimeout(() => {
            set(showStyle);
        }, 100);


    }, [rerenderPoint]);

    return (
        <animated.div style={style} {...other}>
            {
                show && children
            }
        </animated.div>
    )
}