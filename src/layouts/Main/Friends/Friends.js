import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles, useTheme } from '@material-ui/styles';
import Divider from '@material-ui/core/Divider';
import Button from "@material-ui/core/Button";
import { useTranslation } from "react-i18next";
import CheckIcon from '@material-ui/icons/Check';
import useMediaQuery from "@material-ui/core/useMediaQuery";
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Panel from "../../../components/Panel";
import {
    Avatar, CircularProgress,
    IconButton,
    InputBase,
    List,
    ListItem,
    ListItemAvatar,
    ListItemIcon, ListItemSecondaryAction,
    ListItemText,
    Typography
} from "@material-ui/core";
import { useMainContext } from "../context/Main.context";
import SearchIcon from '@material-ui/icons/Search';
import { fade } from '@material-ui/core/styles';
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import { useMutation, useQuery } from "@apollo/client";
import { ADD_FRIEND, CREATE_USER, GET_PROFILE, GET_USERS } from "../../../apollo/requests";

const useStyles = makeStyles(theme => ({
    drawer: {
        width: 400,
        [theme.breakpoints.up('lg')]: {
            //height: 'calc(100% - 96px)',
            borderRadius: 0,
            //border: theme.palette.borderStyle
        },
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%'
    },
    divider: {
        //margin: theme.spacing(2, 0)
    },
    nav: {
        marginBottom: theme.spacing(2)
    },
    logoutBtnIcon: {
        marginRight: theme.spacing(1)
    },
    toolbar: {
        display: "flex",
        flexDirection: 'row-reverse',
        width: '100%',
        alignItems: 'center'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(1),
            width: 'auto',
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${ theme.spacing(4) }px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
                width: '20ch',
            },
        },
    },
}));

const Friends = props => {
    const [value, setValue] = useState('');

    const {data: users, refetch: refetchUsers} = useQuery(GET_USERS, {
        variables: {
            query: value
        }
    })

    const {data: profile, refetch: refetchProfile} = useQuery(GET_PROFILE);

    const [requestFriend, {data: friend}] = useMutation(ADD_FRIEND);

    const {openFriends, setOpenFriends} = useMainContext();

    const {variant, onClose, className, ...rest} = props;

    const classes = useStyles();

    const {t} = useTranslation();

    const theme = useTheme();
    const isDesktop = useMediaQuery(theme.breakpoints.up('lg'), {
        defaultMatches: true
    });

    const clickClose = () => {
        setOpenFriends(false);
    }

    const clickRequestFriend = async (email) => {
        await requestFriend({
            variables: {
                email
            }
        });

        await refetchProfile();
        await refetchUsers({
            query: value
        });
    }

    const getAction = ({_id, email}) => {
        if (profile.getProfile.reqToAllowedFriends.includes(_id)) {
            return <CircularProgress size={36} />
        }


        return profile.getProfile.friends.includes(_id)
            ? (
                <CheckIcon/>
            )
            : (
                <IconButton onClick={e => clickRequestFriend(email)}>
                    <PersonAddIcon/>
                </IconButton>
            )
    }

    return (
        <SwipeableDrawer
            anchor="right"
            classes={ {paper: classes.drawer} }
            onClose={ () => setOpenFriends(false) }
            open={ openFriends }
            variant={ variant }
        >
            <div
                { ...rest }
                className={ clsx(classes.root, className) }
            >
                <Panel toolbarChildren={ (
                    <div className={ classes.toolbar }>
                        <IconButton aria-label="delete" color={ 'inherit' } onClick={ clickClose }>
                            <ChevronRightIcon/>
                        </IconButton>
                        <div className={ classes.search }>
                            <div className={ classes.searchIcon }>
                                <SearchIcon/>
                            </div>
                            <InputBase
                                placeholder="Search…"
                                classes={ {
                                    root: classes.inputRoot,
                                    input: classes.inputInput,
                                } }
                                value={ value }
                                onChange={ e => setValue(e.target.value) }
                                inputProps={ {'aria-label': 'search'} }
                            />
                        </div>
                    </div>
                ) }>
                    <List component="nav" aria-label="main mailbox folders">
                        {
                            users && users.getUsers.map(({_id, name, email}) => (
                                <ListItem button key={ _id }>
                                    <ListItemAvatar>
                                        <Avatar/>
                                    </ListItemAvatar>
                                    <ListItemText secondary={ email }/>
                                    <ListItemSecondaryAction>
                                        {
                                           getAction({_id, name, email})
                                        }
                                    </ListItemSecondaryAction>
                                </ListItem>
                            ))
                        }
                    </List>
                </Panel>
            </div>
        </SwipeableDrawer>
    );
};

Friends.propTypes = {
    className: PropTypes.string,
    onClose: PropTypes.func,
    variant: PropTypes.string.isRequired
};

export default Friends;
