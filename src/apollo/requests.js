import { useQuery, gql } from '@apollo/client';

export const GET_PROFILE = gql`
 {
  getProfile {
    name,
    friends,
    notAllowedFriends,
    reqToAllowedFriends,
    _id
  }
}
`;

export const AUTH = gql`
 query auth($login: String!, $password: String!) {
       auth(input:{
            login: $login
            password: $password
          }), {
            token
       }
 }
`;

export const GET_MESSAGES = gql`
 query messages($roomId: String!, $offset: Float!, $limit: Float!) {
         getMessages(input: {
             roomId: $roomId,
             limit: $limit,
             offset: $offset
        }) {
           text, createAt, userId, _id
        }
 }
`;

export const SUBSCRIPTION_MESSAGES = gql`
 subscription messagePushed($roomId: String!) {
  messagePushed(input: {
    roomId: $roomId
  }) {
    text, createAt, userId
  }
}
`

export const PUSH_MESSAGE = gql`
    mutation pushMessage ($roomId: String!, $text: String!) {
          pushMessage(input:{
            roomId: $roomId,
            text: $text
          }) {
            text, createAt
          }
    }
`

export const CREATE_USER = gql`
mutation createUser($name: String!, $email: String!, $password: String!){
  createUser(input: {
    name: $name,
    email: $email,
    password: $password
  }) {
    _id, confirmToken
  }
}
`;

export const CREATE_CHAT = gql`
mutation createChat($users: [String!]!) {
  createRoom(input: {
    name: "test",
    users: $users
  }) {
    _id
  }
}
`

export const GET_ROOMS = gql`
 query rooms($offset: Float, $limit: Float) {
         getRooms(input: {
             limit: $limit,
             offset: $offset
        }) {
           name, _id, lastMessage
        }
 }
`;


export const GET_USERS = gql`
 query rooms($query: String) {
         getUsers(input: {
             query: $query
        }) {
           name, _id, email
        }
 }
`;

export const ADD_FRIEND = gql`
mutation addFriend($email: String!){
  addFriend(input: {
    email: $email
  }) {
    _id
  }
}
`

export const SUBSCRIPTION_EVENT = gql`
 subscription event($token: String) {
  event(input:{
    token: $token
  }) {
    text, type
  }
}
`