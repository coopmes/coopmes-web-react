import React, { useEffect, useRef, useState } from "react";
import { SpringOpacity, TextField } from "../../components";
import Panel from "../../components/Panel";
import { makeStyles } from "@material-ui/styles";
import { useHistory, useParams } from "react-router";
import { useLazyQuery, useMutation } from "@apollo/client";
import { GET_MESSAGES, GET_ROOMS, PUSH_MESSAGE, SUBSCRIPTION_MESSAGES } from "../../apollo/requests";
import { MessageBox } from 'react-chat-elements'
import { Button, Collapse, Icon, LinearProgress, Typography } from "@material-ui/core";
import SendIcon from '@material-ui/icons/Send';
import { useForm } from "react-hook-form";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        height: 'calc(100vh - 48px)'
    },
    container: {
        overflow: 'auto'
    },
    editor: {
        display: 'flex',
        background: theme.palette.background.paper
    }
}))

const limit = 25;

export default function Messenger_old() {
    console.log('Messenger');

    const {id} = useParams();

    const history = useHistory();

    const [getRooms, { data: rooms, error }] = useLazyQuery(GET_ROOMS);

    const {handleSubmit} = useForm();

    const [getMessages, {data, loading, fetchMore, subscribeToMore}] = useLazyQuery(GET_MESSAGES);

    const [offset, setOffset] = useState(0);

    const [pushMessage] = useMutation(PUSH_MESSAGE);

    const [value, setValue] = useState('');

    const classes = useStyles();

    const bottom = useRef();
    const input = useRef();

    const getRoom = () => {
        if (!rooms) {
            return null;
        }

        return rooms.getRooms.find(r => r._id === id);
    }

    useEffect(() => {
        setOffset(0);

        getRooms({
            variables: {
                offset: 0,
                limit: 50
            }
        });

        getMessages({
            variables: {
                roomId: id,
                offset: 0,
                limit
            }
        })

    }, [id]);

    useEffect(() => {
        try {
            if (subscribeToMore) {

                const unsubscribe = subscribeToMore({
                    document: SUBSCRIPTION_MESSAGES,
                    variables: {
                        roomId: id
                    },
                    updateQuery: (prev, {subscriptionData}) => {
                        if (!subscriptionData.data) {
                            return prev;
                        }

                        const {messagePushed} = subscriptionData.data;

                        return Object.assign({}, prev, {
                            getMessages: [...prev.getMessages, messagePushed]
                        });
                    }
                })

                return () => {
                    unsubscribe()
                }
            }
        } catch (e) {
            console.log(e);
        }
    }, [subscribeToMore, id, data]);

    // useEffect(() => {
    //     if (fetchMore) {
    //         fetchMore({
    //             variables: {
    //                 offset,
    //                 limit: 30
    //             },
    //             updateQuery: (previousResult, { fetchMoreResult, queryVariables }) => {
    //                 return {
    //                     getMessages: [
    //                         ...previousResult.getMessages,
    //                         ...fetchMoreResult.getMessages,
    //                     ],
    //                 };
    //             },
    //         })
    //     }
    // }, [offset, fetchMore]);

    const scrollToBottomContainer = (behavior = 'smooth') => {
        bottom.current.scrollIntoView({behavior});
    }

    useEffect(() => {
        if (data && data.getMessages) {
            scrollToBottomContainer('auto');
        }
    }, [data]);

    const handleSendMessage = async () => {
        if (!value) {
            return;
        }

        await pushMessage({
            variables: {
                roomId: id,
                text: value
            }
        });

        setValue('');
        scrollToBottomContainer();

        input.current.focus();
    }

    const handleScroll = e => {
        if (e.target.scrollTop < 25) {
            setOffset(offset + 25);
        }
    }

    return (
        <SpringOpacity>
            <Panel toolbarChildren={
                getRoom() && (
                    <Typography color={'inherit'} variant={'h4'}>{getRoom().name}</Typography>
                )
            }>
                <Collapse in={ loading }>
                    <LinearProgress color={ 'secondary' }/>
                </Collapse>
                <div className={ classes.root }>
                    <div className={ classes.container } onScroll={ handleScroll }>
                        {
                            data && data.getMessages.map(m => (
                                <MessageBox key={ m._id } title={ m.userId } text={ m.text }
                                            date={ new Date(m.createAt) }/>
                            )).reverse()
                        }
                        <div ref={ bottom } style={ {float: "left", clear: "both"} }></div>
                    </div>
                    <form className={ classes.editor } onSubmit={ handleSubmit(handleSendMessage) }>
                        <TextField value={ value }
                                   label={''}
                                   ref={ input }
                                   placeholder={ 'Type here...' }
                                   onChange={ e => setValue(e.target.value) }></TextField>

                        <Button
                            variant="contained"
                            color="primary"
                            type={ 'submit' }
                            disabled={ !value }
                            className={ classes.button }
                            endIcon={ <SendIcon/> }
                        >
                            Send
                        </Button>
                    </form>
                </div>
            </Panel>
        </SpringOpacity>
    )
}