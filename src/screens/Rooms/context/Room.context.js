import React, {useContext, useState} from "react";
import { useTheme } from "@material-ui/styles";
import { useMediaQuery } from "@material-ui/core";

const RoomContext = React.createContext();

export const useRoomContext = () => {
    return useContext(RoomContext);
}

export function RoomProvider({ children }) {
    const [search, setSearch] = useState('');

    return (
        <RoomContext.Provider value={{
            search,
            setSearch
        }}>
            { children }
        </RoomContext.Provider>
    )
}