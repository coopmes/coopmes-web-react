import React, { useEffect } from "react";
import { useHistory, useParams } from "react-router";
import { useApolloClient } from "@apollo/client";

export default function ConfirmToken() {
    const {token} = useParams();
    const client  = useApolloClient();
    const history = useHistory();

    const confirmEmail = async () => {
        try {
            const resp = await fetch(`${API_URL}/users/confirm/${token}`);
            const jwtToken = await resp.text();

            localStorage.setItem('token', jwtToken);

            await client.clearStore();
            history.replace('/');
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
        confirmEmail();
    }, []);

    return (
        <div>
            confirm
        </div>
    )
}