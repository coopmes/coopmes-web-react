import React from "react";
import { makeStyles } from "@material-ui/styles";
import { Fab, Toolbar } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import { SendField } from "../../../../components";

const useStyles = makeStyles((theme) => ({
    root: {
        top: 'auto',
        bottom: 0,
        flex: 0,
        background: theme.palette.background.second,
    },
    appBar: {
        top: 'auto',
        bottom: 0,
        minHeight: 56,
    },
}));

export default function BottomMessageSend({ value, onChange, onSend }) {
    const classes = useStyles();

    return (
        <div className={ classes.root }>
            <Toolbar className={ classes.appBar }>
                <SendField value={ value } onChange={ onChange } onSend={ onSend }/>
            </Toolbar>
        </div>
    )
}