import React, { Suspense, useState } from "react";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import Header from "./Header";
import Drawer from "./Chats/Drawer";
import { useTheme, makeStyles } from "@material-ui/styles";
import { useMediaQuery } from "@material-ui/core";
import clsx from "clsx";
import Messenger_old from "../../screens/Messenger";
import Settings from "./Settings";
import { useQuery, useSubscription } from "@apollo/client";
import { GET_PROFILE, SUBSCRIPTION_EVENT } from "../../apollo/requests";
import Rooms from "../../screens/Rooms/Rooms";
import EmptyScreen from "../../screens/EmptyScreen";
import Friends from "../../screens/Friends";

const useStyle = makeStyles(theme => ({
    '@global': {
        '*::-webkit-scrollbar': {
            width: '0.4em'
        },
        '*::-webkit-scrollbar-track': {
            '-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,0.00)'
        },
        '*::-webkit-scrollbar-thumb': {
            backgroundColor: 'rgba(0,0,0,.1)'
        },
        'body': {
            background: theme.palette.bg
        }
    },
    root: {
        background: theme.palette.background.default,
        height: '100vh'
    },
    content: {
        background: theme.palette.background.default,
        height: '100vh',
        display: "flex"
    },
    signOutButton: {
        marginLeft: 24
    },
    navbar: {
        display: "flex",
        alignItems: 'center',
        marginLeft: theme.spacing(1)
    },
    shiftContent: {
        paddingLeft: 360,
        //paddingRight: 360
    },
}))

export default function Main() {
    const { data: event } = useSubscription(SUBSCRIPTION_EVENT, {
        variables: {
            token: localStorage.getItem('token')
        }
    })
    const theme = useTheme();
    const isDesktop = useMediaQuery(theme.breakpoints.up('lg'), {
        defaultMatches: true
    });
    const classes = useStyle();

    return (
        <BrowserRouter>
            <div className={ classes.root }>

                <Rooms />

                <Friends />

                <main className={ clsx({
                    [classes.content]: true,
                    [classes.shiftContent]: isDesktop
                }) }>
                    <Suspense fallback={ 'loading' }>
                        <Route render={ ({ location }) => {
                            return (
                                <Switch location={ location }>
                                    <Route path={ '/rooms/notselect' } component={ EmptyScreen } />
                                    <Route path={ '/rooms/:id' } component={ Messenger_old } />

                                    <Redirect from={ '/login' } to={ '/rooms/notselect' }/>
                                    <Redirect from={ '/' } to={ '/rooms/notselect' }/>
                                </Switch>
                            )
                        } }/>
                    </Suspense>
                </main>
            </div>
        </BrowserRouter>
    )
}