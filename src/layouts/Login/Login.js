import React, { useState, Suspense } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from "../../screens/Login";
import Register from '../../screens/Register';
import { Redirect } from "react-router";
import ConfirmEmail from "../../screens/ConfirmEmail";
import ConfirmToken from "../../screens/ConfirmToken";

export default function _Login() {
    return (
        <BrowserRouter>
            <Suspense fallback={ '..loading' }>
                <Route render={ ({ location }) => {
                    return (
                        <Switch location={ location }>
                            <Route path={ '/login' } component={ Login }></Route>
                            <Route path={ '/register' } component={ Register }></Route>
                            <Route path={ '/confirm' } component={ ConfirmEmail }></Route>
                            <Route path={ '/confirmtoken/:token' } component={ ConfirmToken }></Route>

                            <Redirect from={'/'} to={'/login'}></Redirect>
                        </Switch>
                    )
                } }></Route>
            </Suspense>
        </BrowserRouter>
    )
}