import React from "react";
import { makeStyles } from "@material-ui/styles";
import { AppBar, Fab, IconButton, Toolbar } from "@material-ui/core";
import { SearchField } from "../../../../components";
import { useFriendsContext } from "../../context/Friends.context";

const useStyles = makeStyles((theme) => ({
    root: {
        top: 0,
        bottom: 'auto',
        flex: 0,
        background: theme.palette.background.second,
    },
    text: {
        padding: theme.spacing(2, 2, 0),
    },
    paper: {
        paddingBottom: 50,
    },
    list: {
        marginBottom: theme.spacing(2),
    },
    subheader: {
        backgroundColor: theme.palette.background.paper,
    },
    appBar: {
        minHeight: 56,
    },
    grow: {
        flexGrow: 1,
    },
    fabButton: {
        position: 'absolute',
        zIndex: 1,
        top: -30,
        left: 0,
        right: 0,
        margin: '0 auto',
        // boxShadow: `0 0 0 8px ${theme.palette.background.paper}`,
    },
}));

export default function TopBar() {
    const classes = useStyles();
    const { search, setSearch } = useFriendsContext();

    return (
        <div className={ classes.root }>
            <Toolbar className={ classes.appBar }>
                <SearchField value={ search } onChange={ e => setSearch(e.target.value) }/>
            </Toolbar>
        </div>
    )
}