import React from "react";
import { makeStyles } from "@material-ui/styles";
import { useTranslation } from "react-i18next";

import VideCall from '../../images/Video Call.svg';
import { Typography } from "@material-ui/core";
import { SpringSlideUp } from "../../components";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        flexDirection: 'column'
    },
    text: {
        color: theme.palette.text.primary
    },
    slideUp: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column'
    }
}))

export default function EmptyScreen() {
    const classes = useStyles();

    const { t } = useTranslation();

    return (
        <div className={ classes.root }>
            <SpringSlideUp className={ classes.slideUp }>
                <img src={ VideCall } alt=""/>
                <Typography className={ classes.text }
                            variant={ 'h4' }>{ t('Select or create chat to start messaging') }</Typography>
            </SpringSlideUp>
        </div>
    )
}