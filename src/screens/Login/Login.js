import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import { FormControl, Collapse, Typography, Grid, Divider } from '@material-ui/core';
import { useTranslation } from "react-i18next";
import { useForm } from "react-hook-form";
import { Button, ErrorHelper, SpringOpacity, SpringSlideUp, TextField } from "../../components";
import { useHistory } from "react-router";
import { useLazyQuery, useQuery } from "@apollo/client";
import { AUTH, GET_PROFILE } from "../../apollo/requests";

const useStyles = makeStyles(theme => ({
    root: {
        background: theme.palette.background.default,
        [theme.breakpoints.down('md')]: {
            background: theme.palette.background.paper
        },
        display: "flex",
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
    },
    form: {
        display: 'flex',
        background: theme.palette.background.paper,
        [theme.breakpoints.up('md')]: {
            width: 400,
        },
        maxWidth: 600,
        padding: 40,
        flexDirection: 'column'
    },
    grid: {
        flex: 1
    }
}))

export default function Login() {
    const classes = useStyles();
    const {handleSubmit, register, watch} = useForm();
    const [login, { data, error }] = useLazyQuery(AUTH);

    const history = useHistory();

    useEffect(() => {
        if (data && data.auth) {
            localStorage.setItem('token', data.auth.token);
            history.replace('/');
        }
    }, [data]);

    const {t} = useTranslation();

    const onSubmit = async (data) => {
        login({
            variables: data
        })
    }

    const handlerRegister = () => {
        history.push('/register')
    }

    return (
        <div className={ classes.root }>
            <SpringSlideUp>
                <form className={ classes.form } onSubmit={ handleSubmit(onSubmit) }>
                    <Grid className={ classes.grid } container spacing={ 2 }>
                        <Grid item xs={ 12 }>
                            <TextField
                                label={ t('Login') }
                                inputRef={register({
                                    required: "Required"
                                })}
                                name={'login'}/>
                        </Grid>
                        <Grid item xs={ 12 }>
                            <TextField
                                type={ 'password' }
                                inputRef={register({
                                    required: "Required"
                                })}
                                name={'password'}
                                label={ t('Password') }/>
                        </Grid>
                        <Grid item xs={ 12 }>
                            <Button type={'submit'}
                                    color={ 'primary' }
                                    disabled={!(watch('login') && watch('password'))}
                                    fullWidth>Sign in</Button>
                        </Grid>
                        <Grid item xs={ 12 }>
                            <Divider/>
                        </Grid>
                        <Grid item xs={ 12 }>
                            <Button fullWidth color={ 'secondary' } onClick={ handlerRegister }>Register</Button>
                        </Grid>
                        <Grid item xs={12}>
                            <Collapse in={error}>
                                <ErrorHelper>
                                    <Typography color={'inherit'}>{t('Invalid login or password')}</Typography>
                                </ErrorHelper>
                            </Collapse>
                        </Grid>
                    </Grid>
                </form>
            </SpringSlideUp>
        </div>
    )
}