# Linux install

## 1. Install [Docker](https://docs.docker.com/engine/install/)
## 2. Build docker [image](https://docs.docker.com/engine/reference/commandline/image/)
Go to root project folder and execute command: 
```
docker build -t abtesting-real-frontend:current .

```
## 3. Run docker [container](https://docs.docker.com/config/containers/start-containers-automatically/)
Run docker container on port:
```
docker run -p 3000:80 --restart=always --name abtesting-frontend -d abtesting-real-frontend:current

```
